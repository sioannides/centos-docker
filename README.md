centos-docker
========================

Base docker image with CentOS base on official [centos:centos7](https://hub.docker.com/_/centos/) image.

Developed by [Stelios Ioannides](https://stelios.ioannides.info).

### Documentation

The following changes are applied compared to official CentOS image:

* Enable networking
* Install EPEL repo
* Install base tools
* Add bitbucket and github host signatures to SSH known_hosts

### License

[MIT](https://bitbucket.org/sioannides/centos-docker/src/master/LICENSE)

### Links

* Bitbucket: https://bitbucket.org/sioannides/centos-docker
